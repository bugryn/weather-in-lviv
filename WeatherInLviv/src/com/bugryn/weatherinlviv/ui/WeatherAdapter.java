package com.bugryn.weatherinlviv.ui;

import java.util.List;

import com.bugryn.weatherinlviv.utils.WeatherForDay;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class WeatherAdapter extends ArrayAdapter<WeatherForDay> {

    private Context context;
    private int layoutResourceId;
    private List<WeatherForDay> data;

    public WeatherAdapter(Context context, int layoutResourceId, List<WeatherForDay> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        WeatherHolder weatherHolder = null;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(layoutResourceId, parent, false);
            weatherHolder = new WeatherHolder(view);
            view.setTag(weatherHolder);
        } else {
            weatherHolder = (WeatherHolder) view.getTag();
        }

        WeatherForDay weather = data.get(position);
        fillWeatherHolder(weatherHolder, weather);

        return view;
    }

    private void fillWeatherHolder(WeatherHolder weatherHolder, WeatherForDay weather) {

        if (weather == null) {
        } else {
            weatherHolder.getDayTitle().setText(weatherHolder.convertDatetoDayName(weather.getDate()));
            weatherHolder.getDateTitle().setText(weather.getDate());
            weatherHolder.getWeatherInfo().setText(weather.getWeatherDesc().get(0).getWeatherDesc());
            weatherHolder.getMaxT().setText(Integer.toString(weather.getTempMaxC()));
            weatherHolder.getMinT().setText(Integer.toString(weather.getTempMinC()));

            if (weather.getWeatherIcon() == null) {
                String imageUrl = weather.getWeatherIconUrl().get(0).getWeatherIconUrl();
                if (imageUrl != null && imageUrl.length() > 0) {
                    DownloadWeatherIconTask imageLoaderTask = new DownloadWeatherIconTask();
                    imageLoaderTask.execute(imageUrl, weatherHolder.getWeatherImageIcon(), weather);
                }
            } else {
                weatherHolder.getWeatherImageIcon().setImageBitmap(weather.getWeatherIcon());
            }
        }

    }

}
