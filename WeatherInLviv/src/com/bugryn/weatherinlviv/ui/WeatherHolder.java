package com.bugryn.weatherinlviv.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bugryn.weatherinlviv.R;
import com.bugryn.weatherinlviv.utils.Logger;

public class WeatherHolder {

    private final String WEATHER_HOLDER_TAG = this.getClass().getSimpleName();

    private ImageView weatherImageIcon;
    private TextView dayTitle;
    private TextView dateTitle;
    private TextView weatherInfo;
    private TextView maxT;
    private TextView minT;

    public WeatherHolder() {
        super();
    }

    public WeatherHolder(View view) {
        weatherImageIcon = (ImageView) view.findViewById(R.id.weather_icon);
        dayTitle = (TextView) view.findViewById(R.id.day_title);
        dateTitle = (TextView) view.findViewById(R.id.date_title);
        weatherInfo = (TextView) view.findViewById(R.id.weather_info);
        maxT = (TextView) view.findViewById(R.id.max_T);
        minT = (TextView) view.findViewById(R.id.min_T);
    }

    public WeatherHolder(ImageView weatherImageIcon, TextView dayTitle, TextView dateTitle, TextView weatherInfo,
            TextView maxT, TextView minT) {
        this.weatherImageIcon = weatherImageIcon;
        this.dayTitle = dayTitle;
        this.dateTitle = dateTitle;
        this.weatherInfo = weatherInfo;
        this.maxT = maxT;
        this.minT = minT;
    }

    public ImageView getWeatherImageIcon() {
        return weatherImageIcon;
    }

    public void setWeatherImageIcon(ImageView weatherImageIcon) {
        this.weatherImageIcon = weatherImageIcon;
    }

    public TextView getDayTitle() {
        return dayTitle;
    }

    public void setDayTitle(TextView dayTitle) {
        this.dayTitle = dayTitle;
    }

    public TextView getDateTitle() {
        return dateTitle;
    }

    public void setDateTitle(TextView dateTitle) {
        this.dateTitle = dateTitle;
    }

    public TextView getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(TextView weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    public TextView getMaxT() {
        return maxT;
    }

    public void setMaxT(TextView maxT) {
        this.maxT = maxT;
    }

    public TextView getMinT() {
        return minT;
    }

    public void setMinT(TextView minT) {
        this.minT = minT;
    }

    public String convertDatetoDayName(String dateStr) {
        final SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = null;
        try {
            date = inFormat.parse(dateStr);
        } catch (ParseException e) {
            Logger.logError(WEATHER_HOLDER_TAG, "Oops, Something wrong with date  parsing... " + e.getMessage());
        }
        final SimpleDateFormat outFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        return outFormat.format(date);
    }
}