package com.bugryn.weatherinlviv.ui;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.bugryn.weatherinlviv.utils.ImageCache;
import com.bugryn.weatherinlviv.utils.Loader;
import com.bugryn.weatherinlviv.utils.Logger;
import com.bugryn.weatherinlviv.utils.WeatherForDay;

class DownloadWeatherIconTask extends AsyncTask<Object, Void, Bitmap> {

    private final String DOWNLOAD_WEATHER_ICON_TASK = this.getClass().getSimpleName();
    private ImageView loadedImageView;
    private Loader imageLoader = new Loader();
    private WeatherForDay weather;
    private Boolean isDownload = false;

    @Override
    protected Bitmap doInBackground(Object... params) {
        String url = "";
        if (params.length > 0) {
            url = (String) params[0];
        }

        loadedImageView = (ImageView) params[1];
        weather = (WeatherForDay) params[2];

        Bitmap loadedImageBtm = null;

        synchronized (weather.getDownloadIconLock()) {
            if (weather.getWeatherIcon() == null) {
                try {
                    Logger.logInfo(DOWNLOAD_WEATHER_ICON_TASK, "Start loading image ");
                    loadedImageBtm = ImageCache.getInstance().readBitmapFromCache(url);
                    if (loadedImageBtm == null) {
                        loadedImageBtm = imageLoader.downloadBitmapByUrl(url);
                        if (loadedImageBtm == null) {
                            isDownload = false;
                        } else {
                            isDownload = true;
                            ImageCache.getInstance().saveBitmapInCahce(loadedImageBtm, url);
                        }
                    } else {
                        Logger.logInfo(DOWNLOAD_WEATHER_ICON_TASK, "Get bmp from cache " + url);
                        isDownload = true;
                    }

                    weather.setWeatherIcon(loadedImageBtm);

                } catch (Exception e) {
                    Logger.logError(DOWNLOAD_WEATHER_ICON_TASK, e.getMessage());
                }
            } else {
                isDownload = true;
                return null;
            }
        }

        return loadedImageBtm;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        if (isCancelled()) {
            Logger.logInfo(DOWNLOAD_WEATHER_ICON_TASK, "Download task was canceled");
        } else if (result == null) {
            if (isDownload) {
                Logger.logInfo(DOWNLOAD_WEATHER_ICON_TASK, "image already downloaded");
            } else {
                Logger.logInfo(DOWNLOAD_WEATHER_ICON_TASK, "Can't download image");
            }
        } else {
            Logger.logInfo(DOWNLOAD_WEATHER_ICON_TASK, "Download image");
            loadedImageView.setImageBitmap(result);
        }
    }

}
