package com.bugryn.weatherinlviv.utils;

public class MainJSON {
    private Data data;

    public Data getMainJSONData() {
        return data;
    }

    public void setMainJSONData(Data data) {
        this.data = data;
    }
}
