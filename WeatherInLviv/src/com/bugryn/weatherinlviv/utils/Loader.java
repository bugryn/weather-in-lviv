package com.bugryn.weatherinlviv.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;

public class Loader {

    private final String IMAGE_LOADER_TAG = this.getClass().getSimpleName();

    public Bitmap downloadBitmapByUrl(String url) {

        if (url == null || url.length() == 0) {
            return null;
        } else {

            final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
            final HttpGet getImageRequest = new HttpGet(url);

            try {
                HttpResponse response = httpClient.execute(getImageRequest);

                if (!checkResponseStatus(response, url)) {
                    return null;
                }

                final HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream inputStream = null;
                    try {
                        inputStream = entity.getContent();
                        final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                        return bitmap;
                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        entity.consumeContent();
                    }
                }
            } catch (Exception e) {
                Logger.logError(IMAGE_LOADER_TAG, "Error " + e.getMessage());
                getImageRequest.abort();
            } finally {
                if (httpClient != null) {
                    httpClient.close();
                }
            }
            return null;
        }
    }

    public List<WeatherForDay> downloadWeahterForDays(String url) {

        if (url == null || url.length() == 0) {
            return null;
        } else {

            final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
            final HttpGet getJsonRequest = new HttpGet(url);

            try {
                HttpResponse response = httpClient.execute(getJsonRequest);

                if (!checkResponseStatus(response, url)) {
                    return null;
                }

                final HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream inputStream = null;
                    InputStreamReader jsonInputStreamReader = null;
                    try {
                        inputStream = entity.getContent();
                        WeatherJSONReader reader = new WeatherJSONReader();
                        if (inputStream != null) {
                            jsonInputStreamReader = new InputStreamReader(inputStream);
                            List<WeatherForDay> weatherForDayList = reader.readWeatherFromStream(jsonInputStreamReader);
                            return weatherForDayList;
                        } else {
                            return null;
                        }
                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                        if (jsonInputStreamReader != null) {
                            jsonInputStreamReader.close();
                        }

                        entity.consumeContent();
                    }
                }
            } catch (Exception e) {
                Logger.logError(IMAGE_LOADER_TAG, "Error " + e.getMessage());
                getJsonRequest.abort();
            } finally {
                if (httpClient != null) {
                    httpClient.close();
                }
            }
            return null;
        }
    }

    private Boolean checkResponseStatus(HttpResponse response, String url) {
        final int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            Logger.logError(IMAGE_LOADER_TAG, "Error " + statusCode + " while retrieving resource from " + url);
            return false;
        } else {
            return true;
        }
    }

}
