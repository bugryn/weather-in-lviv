package com.bugryn.weatherinlviv.utils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class WeatherJSONReader {

    public List<WeatherForDay> readWeatherFromStream(InputStreamReader jsonStream) throws IOException {
        MainJSON data = readJsonStream(jsonStream);
        return data == null ? null : data.getMainJSONData().getWeatherForDay();
    }

    public MainJSON readJsonStream(InputStreamReader jsonStream) throws IOException {
        if (jsonStream == null) {
            return null;
        }
        final Gson gson = new Gson();
        final JsonReader reader = new JsonReader(jsonStream);
        MainJSON data = gson.fromJson(reader, MainJSON.class);
        try {
            return data;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

}
