package com.bugryn.weatherinlviv.utils;

public class WeatherIconUrl {
    private String value;

    public String getWeatherIconUrl() {
        return value;
    }

    public void setWeatherIconUrl(String weatherIconUrl) {
        this.value = weatherIconUrl;
    }

}
