package com.bugryn.weatherinlviv.utils;

import android.util.Log;

public class Logger {

    public static void logError(String TAG, String msg) {
        Log.e(TAG, msg);
    }

    public static void logInfo(String TAG, String msg) {
        Log.i(TAG, msg);
    }
}
