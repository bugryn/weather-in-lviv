package com.bugryn.weatherinlviv.utils;

import java.util.List;

import android.graphics.Bitmap;

public class WeatherForDay {

    private String date;
    private Integer tempMaxC;
    private Integer tempMaxF;
    private Integer tempMinC;
    private Integer tempMinF;
    private List<WeatherDesc> weatherDesc;
    private List<WeatherIconUrl> weatherIconUrl;
    private Bitmap weatherIcon;
    private Object downloadIconLock;

    public WeatherForDay() {
        downloadIconLock = new Object();
    }

    public Object getDownloadIconLock() {
        return downloadIconLock;
    }

    public void setDownloadIconLock(Object downloadIconLock) {
        this.downloadIconLock = downloadIconLock;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTempMaxC() {
        return tempMaxC;
    }

    public void setTempMaxC(Integer tempMaxC) {
        this.tempMaxC = tempMaxC;
    }

    public Integer getTempMaxF() {
        return tempMaxF;
    }

    public void setTempMaxF(Integer tempMaxF) {
        this.tempMaxF = tempMaxF;
    }

    public Integer getTempMinC() {
        return tempMinC;
    }

    public void setTempMinC(Integer tempMinC) {
        this.tempMinC = tempMinC;
    }

    public Integer getTempMinF() {
        return tempMinF;
    }

    public void setTempMinF(Integer tempMinF) {
        this.tempMinF = tempMinF;
    }

    public List<WeatherDesc> getWeatherDesc() {
        return weatherDesc;
    }

    public void setWeatherDesc(List<WeatherDesc> weatherDesc) {
        this.weatherDesc = weatherDesc;
    }

    public List<WeatherIconUrl> getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public void setWeatherIconUrl(List<WeatherIconUrl> weatherIconUrl) {
        this.weatherIconUrl = weatherIconUrl;
    }

    public Bitmap getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(Bitmap weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

}
