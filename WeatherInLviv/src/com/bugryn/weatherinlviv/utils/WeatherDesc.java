package com.bugryn.weatherinlviv.utils;

public class WeatherDesc {
    private String value;

    public String getWeatherDesc() {
        return value;
    }

    public void setWeatherDesc(String weatherDesc) {
        this.value = weatherDesc;
    }
}
