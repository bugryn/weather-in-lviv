package com.bugryn.weatherinlviv.utils;

import java.util.List;

public class Data {
    private List<WeatherForDay> weather;

    public List<WeatherForDay> getWeatherForDay() {
        return weather;
    }

    public void setWeatherForDay(List<WeatherForDay> weather) {
        this.weather = weather;
    }
}
