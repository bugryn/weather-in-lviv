package com.bugryn.weatherinlviv.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.webkit.URLUtil;

public class ImageCache {

    private final String IMAGE_CACHE_TAG = this.getClass().getSimpleName();
    private Context context;
    private static ImageCache imageCache;

    public static void init(Context context) {
        if (context != null) {
            imageCache = new ImageCache(context);
        }
    }

    public static ImageCache getInstance() {
        return imageCache;
    }

    private ImageCache(Context context) {
        this.context = context;
    }

    public void saveBitmapInCahce(Bitmap bmp, String url) throws FileNotFoundException {
        String chaedBtmName = URLUtil.guessFileName(url, null, "image/png");
        String cacheDir = context.getCacheDir().getPath();
        FileOutputStream cachedBmpOutStream = null;

        try {
            File cachedBtm = new File(cacheDir, chaedBtmName);
            cachedBmpOutStream = new FileOutputStream(cachedBtm);

            Logger.logInfo(IMAGE_CACHE_TAG, "Start writing image in cache " + chaedBtmName);
            bmp.compress(Bitmap.CompressFormat.PNG, 0, cachedBmpOutStream);
            Logger.logInfo(IMAGE_CACHE_TAG, "End writing image in cache " + chaedBtmName);

        } finally {
            if (cachedBmpOutStream != null) {
                try {
                    cachedBmpOutStream.flush();
                    cachedBmpOutStream.close();
                } catch (IOException e) {
                    Logger.logError(IMAGE_CACHE_TAG, e.getMessage());
                }
            }
        }
    }

    public Bitmap readBitmapFromCache(String bmpUrl) {
        String chaedBtmName = URLUtil.guessFileName(bmpUrl, null, "image/png");
        String cacheDir = context.getCacheDir().getPath();

        File cachedBtmFile = new File(cacheDir, chaedBtmName);
        Bitmap bitmap = null;
        FileInputStream cachedBmpInStream = null;

        try {
            if (cachedBtmFile.exists()) {
                cachedBmpInStream = new FileInputStream(cachedBtmFile);
                if (cachedBmpInStream.available() == 0) {
                    Logger.logInfo(IMAGE_CACHE_TAG, "Can't read image from cache " + chaedBtmName);
                    return null;
                } else {
                    bitmap = BitmapFactory.decodeStream(cachedBmpInStream);
                    Logger.logInfo(IMAGE_CACHE_TAG, "Read image from cache " + chaedBtmName);
                }
            } else {
                return null;
            }
        } catch (FileNotFoundException e) {
            Logger.logError(IMAGE_CACHE_TAG, e.getMessage());
        } catch (IOException e) {
            Logger.logError(IMAGE_CACHE_TAG, e.getMessage());
        } finally {
            try {
                if (cachedBmpInStream != null) {
                    cachedBmpInStream.close();
                }
            } catch (IOException e) {
                Logger.logError(IMAGE_CACHE_TAG, e.getMessage());
            }
        }
        return bitmap;
    }

    public void clearApplicationCache() {
        File cacheDir = context.getCacheDir();
        File appDir = new File(cacheDir.getParent());
        if (appDir.exists()) {
            String[] subDirs = appDir.list();
            for (String s : subDirs) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                }
            }
        }
    }

    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] subDirs = dir.list();
            for (int i = 0; i < subDirs.length; i++) {
                boolean success = deleteDir(new File(dir, subDirs[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
}
