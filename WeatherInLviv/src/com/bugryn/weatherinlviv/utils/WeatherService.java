package com.bugryn.weatherinlviv.utils;

import java.io.IOException;
import java.util.List;

public class WeatherService {

    private final static String WEATHER_SERVICE_LVIV_BASE_URL = "http://api.worldweatheronline.com/free/v1/weather.ashx?q=Lviv&format=json";
    private final static String WEATHER_SERVICE_NUMBER_OF_DAYS = "&num_of_days=";
    private final static String WEATHER_SERVICE_APPLICATION_KEY = "&key=";

    public List<WeatherForDay> getWeather(int numbOfDays, String weatherAPIKey) throws IOException {

        if (weatherAPIKey == null || weatherAPIKey.length() == 0 || numbOfDays < 1) {
            return null;
        }

        final Loader loader = new Loader();
        String getWeatherURL = generateGetWeatherURL(numbOfDays, weatherAPIKey);
        List<WeatherForDay> weatherForDayList = loader.downloadWeahterForDays(getWeatherURL);

        return weatherForDayList;
    }

    private String generateGetWeatherURL(int numbOfDays, String weatherAPIKey) {
        String getWeatherURL = WEATHER_SERVICE_LVIV_BASE_URL;
        getWeatherURL += WEATHER_SERVICE_NUMBER_OF_DAYS + numbOfDays;
        getWeatherURL += WEATHER_SERVICE_APPLICATION_KEY + weatherAPIKey;

        return getWeatherURL;
    }
}
