package com.bugryn.weatherinlviv.db;

/**
 * @author Yuriy Bugryn.
 */

public class WeatherModel {
    private String date;
    private Integer tempMaxC;
    private Integer tempMaxF;
    private Integer tempMinC;
    private Integer tempMinF;
    private Integer dayNumber;
    private String weatherDesc;

    public void setWeatherDesc(String weatherDesc) {
        this.weatherDesc = weatherDesc;
    }

    public String getWeatherDesc() {
        return weatherDesc;
    }

    public WeatherModel() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTempMaxC() {
        return tempMaxC;
    }

    public void setTempMaxC(Integer tempMaxC) {
        this.tempMaxC = tempMaxC;
    }

    public Integer getTempMaxF() {
        return tempMaxF;
    }

    public void setTempMaxF(Integer tempMaxF) {
        this.tempMaxF = tempMaxF;
    }

    public Integer getTempMinC() {
        return tempMinC;
    }

    public void setTempMinC(Integer tempMinC) {
        this.tempMinC = tempMinC;
    }

    public Integer getTempMinF() {
        return tempMinF;
    }

    public void setTempMinF(Integer tempMinF) {
        this.tempMinF = tempMinF;
    }

    public Integer getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(Integer dayNumber) {

        this.dayNumber = dayNumber;
    }

}
