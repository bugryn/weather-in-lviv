package com.bugryn.weatherinlviv.db;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class WeatherContentProvider extends ContentProvider {

    private SQLiteWeatherDbHelper database;

    private static final int ALL_WEATHER = 1;
    private static final int SINGLE_WEATHER = 2;
    private static final int DAY_NUMBER = 3;

    private static final String AUTHORITY = "com.bugryn.weatherinlviv.db";

    private static final String BASE_PATH = "weather";
    private static final String BY_DAY_NUMBER = "by_day_numb";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/weather";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/weather";

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, ALL_WEATHER);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", SINGLE_WEATHER);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/" + BY_DAY_NUMBER + "/#", DAY_NUMBER);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public boolean onCreate() {
        database = new SQLiteWeatherDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        checkColumns(projection);

        queryBuilder.setTables(SQLiteWeatherCommandHelper.TABLE_NAME);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
        case ALL_WEATHER:
            if (TextUtils.isEmpty(sortOrder))
                sortOrder = "_ID ASC";
            break;
        case SINGLE_WEATHER:
            queryBuilder.appendWhere(SQLiteWeatherCommandHelper._ID + "=" + uri.getLastPathSegment());
            break;
        case DAY_NUMBER:
            queryBuilder.appendWhere(SQLiteWeatherCommandHelper.WEATHER_DAY_NUMB + "=" + uri.getLastPathSegment());
            break;
        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    private void checkColumns(String[] projection) {
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(WeatherDataSource.allColumns));

            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }

}
