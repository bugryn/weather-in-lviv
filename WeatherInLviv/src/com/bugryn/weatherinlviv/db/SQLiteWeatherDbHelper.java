package com.bugryn.weatherinlviv.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Yuriy Bugryn.
 */

public class SQLiteWeatherDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 8;
    public static final String DATABASE_NAME = "WeatherInLviv.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
                    "CREATE TABLE " + SQLiteWeatherCommandHelper.TABLE_NAME + " (" +
                    SQLiteWeatherCommandHelper._ID + " INTEGER PRIMARY KEY," +
                    SQLiteWeatherCommandHelper.WEATHER_DATE + TEXT_TYPE + COMMA_SEP +
                    SQLiteWeatherCommandHelper.WEATHER_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MAX_C + INTEGER_TYPE + COMMA_SEP +
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MIN_C + INTEGER_TYPE + COMMA_SEP +
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MIN_F + INTEGER_TYPE + COMMA_SEP +
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MAX_F + INTEGER_TYPE + COMMA_SEP +
                    SQLiteWeatherCommandHelper.WEATHER_DAY_NUMB + INTEGER_TYPE +
            " )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + SQLiteWeatherCommandHelper.TABLE_NAME;

    public SQLiteWeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void clearWeatherTable(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

}
