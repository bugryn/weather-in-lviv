package com.bugryn.weatherinlviv.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yuriy Bugryn.
 */

public class WeatherDataSource {

    private SQLiteDatabase database;
    private SQLiteWeatherDbHelper dbHelper;
    public static final String[] allColumns = {
            SQLiteWeatherCommandHelper._ID ,
                    SQLiteWeatherCommandHelper.WEATHER_DATE,
                    SQLiteWeatherCommandHelper.WEATHER_DESCRIPTION,
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MAX_C,
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MIN_C,
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MIN_F,
                    SQLiteWeatherCommandHelper.WEATHER_TEMP_MAX_F,
                    SQLiteWeatherCommandHelper.WEATHER_DAY_NUMB
    };

    public WeatherDataSource(Context context) {
        dbHelper = new SQLiteWeatherDbHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long insertWeatherForDay(WeatherModel weather) {
        ContentValues values = new ContentValues();
        values.put(SQLiteWeatherCommandHelper.WEATHER_DATE, weather.getDate());
        values.put(SQLiteWeatherCommandHelper.WEATHER_DESCRIPTION, weather.getWeatherDesc());
        values.put(SQLiteWeatherCommandHelper.WEATHER_TEMP_MAX_C, weather.getTempMaxC());
        values.put(SQLiteWeatherCommandHelper.WEATHER_TEMP_MIN_C, weather.getTempMinC());
        values.put(SQLiteWeatherCommandHelper.WEATHER_TEMP_MIN_F, weather.getTempMinF());
        values.put(SQLiteWeatherCommandHelper.WEATHER_TEMP_MAX_F, weather.getTempMaxF());
        values.put(SQLiteWeatherCommandHelper.WEATHER_DAY_NUMB, weather.getDayNumber());

        long insertId = database.insert(SQLiteWeatherCommandHelper.TABLE_NAME, null, values);

        return insertId;
    }

    public void insertWeatherForDayList(List<WeatherModel> weatherList) {
        if (weatherList != null) {
            dbHelper.clearWeatherTable(database);

            for (WeatherModel weather : weatherList) {
                insertWeatherForDay(weather);
            }
        }
    }

    public List<WeatherModel> getAllWeather() {
        List<WeatherModel> weatherList = new ArrayList<WeatherModel>();

        Cursor cursor = database.query(SQLiteWeatherCommandHelper.TABLE_NAME, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            WeatherModel weather = cursorToWeather(cursor);
            weatherList.add(weather);
            cursor.moveToNext();
        }

        cursor.close();
        return weatherList;
    }

    private WeatherModel cursorToWeather(Cursor cursor) {
        WeatherModel weather = new WeatherModel();
        weather.setDate(cursor.getString(1));
        weather.setWeatherDesc(cursor.getString(2));
        weather.setTempMaxC(cursor.getInt(3));
        weather.setTempMinC(cursor.getInt(4));
        weather.setTempMaxF(cursor.getInt(5));
        weather.setTempMinF(cursor.getInt(6));
        weather.setDayNumber(cursor.getInt(7));
        return weather;
    }
}
