package com.bugryn.weatherinlviv.db;

import android.provider.BaseColumns;

/**
 * @author Yuriy Bugryn.
 */

public class SQLiteWeatherCommandHelper implements BaseColumns {
    public static final String TABLE_NAME = "weather_info";
    public static final String WEATHER_DATE = "date";
    public static final String WEATHER_TEMP_MAX_C = "temp_max_c";
    public static final String WEATHER_TEMP_MIN_C = "temp_min_c";
    public static final String WEATHER_TEMP_MAX_F = "temp_max_f";
    public static final String WEATHER_TEMP_MIN_F = "temp_min_f";
    public static final String WEATHER_DESCRIPTION = "weather_desc";
    public static final String WEATHER_DAY_NUMB = "weather_day_numb";
}
