package com.bugryn.weatherinlviv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bugryn.weatherinlviv.db.WeatherDataSource;
import com.bugryn.weatherinlviv.db.WeatherModel;
import com.bugryn.weatherinlviv.ui.WeatherAdapter;
import com.bugryn.weatherinlviv.utils.ImageCache;
import com.bugryn.weatherinlviv.utils.Logger;
import com.bugryn.weatherinlviv.utils.WeatherForDay;
import com.bugryn.weatherinlviv.utils.WeatherService;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class WeatherBoardActivity extends Activity {

    private WeatherService weatherService;
    private WeatherAdapter weatherListViewAdapter;
    private RelativeLayout weatherLoadingPanel;
    private WeatherDataSource weatherDataSource;

    private String WEATHER_ACTIVITY_TAG = this.getClass().getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_board);
        weatherDataSource = new WeatherDataSource(this);

        weatherLoadingPanel = (RelativeLayout) findViewById(R.id.weather_loading_panel);
        List<WeatherForDay> weatherForDay = new ArrayList<WeatherForDay>();
        weatherListViewAdapter = new WeatherAdapter(this, R.layout.listview_item_row, weatherForDay);
        ListView weatherListView = (ListView) findViewById(R.id.weather_list_view);
        weatherListView.setAdapter(weatherListViewAdapter);

        ImageCache.init(this);
        weatherService = new WeatherService();

        loadWeather();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.weather_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_refresh:
            refreshWeather();
            break;
        case R.id.action_clear_cache:
            ImageCache.getInstance().clearApplicationCache();
            break;
        }
        return true;

    }

    private void loadWeather() {

        if (isInternetConnectingAvailable()) {
            DownloadWeatherJsonTask downloadWeatherJson = new DownloadWeatherJsonTask();
            weatherLoadingPanel.setVisibility(View.VISIBLE);
            downloadWeatherJson.execute();
        } else {
            showProblemAlertDialog(this, "No Internet Connection", "You, don't have internet connection");
        }
    }

    private void refreshWeather() {
        if (weatherListViewAdapter != null) {
            weatherListViewAdapter.clear();
            loadWeather();
        }
    }

    public boolean isInternetConnectingAvailable() {
        ConnectivityManager connectManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectManager != null) {
            NetworkInfo[] networkInfo = connectManager.getAllNetworkInfo();
            if (networkInfo != null)
                for (int i = 0; i < networkInfo.length; i++)
                    if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public void showProblemAlertDialog(Context context, String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);

        alertDialog.setIcon(R.drawable.ic_error);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
            }
        });

        alertDialog.show();
    }

    public void fillWeatherList(List<WeatherForDay> weatherForDay) {
        if (weatherForDay == null || weatherForDay.size() < 1) {

            if (isInternetConnectingAvailable()) {
                showProblemAlertDialog(this, "Can't download information","Can't download weather information from service");
            } else {
                showProblemAlertDialog(this, "No Internet Connection", "You, don't have internet connection");
            }

        } else {
            weatherListViewAdapter.clear();
            for (WeatherForDay weatherForoneDay : weatherForDay) {
                weatherListViewAdapter.add(weatherForoneDay);
            }
            weatherListViewAdapter.notifyDataSetChanged();
        }
    }

    private List<WeatherModel> weatherForDayListToWeatherModelList (List<WeatherForDay> weatherForDayList){
        List<WeatherModel> weatherModelList = new ArrayList<WeatherModel>();
        if(weatherForDayList != null){
            for(int i = 0; i<weatherForDayList.size();i++){
                WeatherModel weatherModel = new WeatherModel();
                weatherModel.setDayNumber(i);
                weatherModel.setDate(weatherForDayList.get(i).getDate());
                weatherModel.setWeatherDesc(weatherForDayList.get(i).getWeatherDesc().get(0).getWeatherDesc());
                weatherModel.setTempMaxC(weatherForDayList.get(i).getTempMaxC());
                weatherModel.setTempMinC(weatherForDayList.get(i).getTempMinC());
                weatherModel.setTempMaxF(weatherForDayList.get(i).getTempMaxF());
                weatherModel.setTempMinF(weatherForDayList.get(i).getTempMinC());
                weatherModelList.add(weatherModel);
            }
        }
        return weatherModelList;
    }

    class DownloadWeatherJsonTask extends AsyncTask<Void, Void, List<WeatherForDay>> {

        @Override
        protected List<WeatherForDay> doInBackground(Void... params) {
            List<WeatherForDay> weatherForDay = null;
            try {
                weatherForDay = weatherService.getWeather(7, "6vvfybbh4wzhwh4tjk3gwxcp");
            } catch (IOException e) {
                Logger.logError(WEATHER_ACTIVITY_TAG, e.getMessage());
            }
            return weatherForDay;
        }

        @Override
        protected void onPostExecute(List<WeatherForDay> result) {
            super.onPostExecute(result);
            weatherLoadingPanel.setVisibility(View.GONE);

            try {
                weatherDataSource.open();
                weatherDataSource.insertWeatherForDayList(weatherForDayListToWeatherModelList(result));
            } catch (SQLException e) {
                Logger.logError(WEATHER_ACTIVITY_TAG, "DB error " + e.getMessage());
            } finally {
                weatherDataSource.close();
            }

            fillWeatherList(result);
        }
    }
}
